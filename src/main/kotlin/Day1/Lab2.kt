import java.lang.IndexOutOfBoundsException
import java.time.temporal.TemporalAmount

var funds : Double = 100.0
val pswd = "password"

fun main() {
    var input : String
    var cmd : List<String>

    while (true) {
        print("Command: ")
        input = readLine()!!
        cmd = input.split(" ")
        when (cmd[0]) {
            // Each command goes here...
            "balance" -> balance()
            "deposit" -> try {
                deposit(cmd[1].toDouble())
            } catch (ex: IndexOutOfBoundsException) {
                println("Invalid amount")
            } catch (ex: NumberFormatException){
                println("Invalid amount")
            }
            "withdraw" -> try {
                withdraw(cmd[1].toDouble())
            } catch (exception: IndexOutOfBoundsException){
                println("Invalid amount")
            } catch (ex: NumberFormatException){
                println("Invalid amount")
            }
            else -> println("Invalid command")
        }
    }
}

fun balance() = println(funds)

fun deposit(amount: Double) {
    println(amount)
    funds += amount
}

fun withdraw(amount: Double) {
    print("Please input your password: ")
    var password = readLine();
    if (password == pswd){
        funds -= amount
    } else {
        println("Invalid password, try withdrawing again")
    }
}