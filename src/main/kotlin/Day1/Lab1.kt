fun main(args: Array<String>){
//    println("Hello, World.")

//    for (i in -5..5)
//        println(calculateY(3, i, -1))

//    for (i in 0..20){
//        var y1 = calculateY(3, i, 5)
//        var y2 = calculateY(4, i, 2)
//        if (y1 == y2){
//            println("($i, $y1)")
//        }
//    }
//    intersection(3, 5, 4, 2)
//    intersection(2, 1, 3, 3)
    intersection(2, 3, 3, -2)
}

fun calculateY(m : Int, x : Int, c : Int) : Int {
    // y = mx + c - formula for a straight line
    return m * x + c
}

// This function takes the equations of two lines:
// yOne = mOne * x + cOne and yTwo = mTwo * x + cTwo
// and prints the X value they intersect at
// or prints "Don't intersect" if they do not.
// You only need to check x values between 0 and 100
fun intersection(mOne : Int, cOne : Int, mTwo : Int, cTwo : Int) : Unit {
    for (i in 0..100){
        var y1 = calculateY(mOne, i, cOne)
        var y2 = calculateY(mTwo, i, cTwo)
        if (y1 == y2){
            println("($i, $y1)")
        } else {
            println("Don't match")
        }
    }
}