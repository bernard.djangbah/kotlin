package ookotlin

import ookotlin.action.Action
import ookotlin.action.ForageAction
import ookotlin.actor.Actor

class FoodEnvironment(vararg args: Actor) : Environment(*args) {
    val scores = mutableMapOf<Actor, Int>()

    init {
        for (arg in args){
            scores[arg] = 0;
        }
    }
    override fun processAction(agent: Actor, act: Action) {
        if (act is ForageAction){
            scores[agent] = scores.getOrDefault(agent, 0) + 1;
        }
    }

    override fun sense(agent: Actor) {
    }
}