package ookotlin

import ookotlin.actor.Actor
import ookotlin.actor.SimpleAgent

fun main(args: Array<String>){
    val env = FoodEnvironment(SimpleAgent("Charlie"))
    env.step()
    println(env.scores)
//    var myself = SimpleAgent("Bernard");
//    println(myself)
//    var env = FoodEnvironment();
}