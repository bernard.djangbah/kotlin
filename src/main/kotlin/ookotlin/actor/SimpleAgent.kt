package ookotlin.actor

import ookotlin.Percept
import ookotlin.action.Action
import ookotlin.action.ForageAction

class SimpleAgent(override val name: String) : Actor {
    var action = ForageAction();
    override fun act(): Action {
        return action
    }

    override fun perceive(vararg facts: Percept) {
    }

    override fun toString(): String {
        return "SimpleAgent(name='$name')"
    }


}