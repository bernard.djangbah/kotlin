package ookotlin.actor

import ookotlin.Percept
import ookotlin.action.Action

interface Actor {
    val name : String
    fun act() : Action
    fun perceive(vararg facts : Percept) : Unit
}